# CHANGELOG

## 2018-07-29 -- version 0.2

- Make sure Insomnia's urlbar is loaded before appending Xdebug checkbox (issue #3)

## 2017-11-03 -- version 0.1

- Creation of the **insomnia-plugin-xdebug*