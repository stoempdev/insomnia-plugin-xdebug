class InsomniaXdebugPlugin {
    constructor() {
        this.xdebugSessionKey = 'Insomnia';
        this.destruct();
    }

    initPlugin() {
        let self = this;
        let input = document.createElement('input');
        input.type = 'checkbox';
        input.name = 'xdebug-enabled';
        input.value = 1;
        input.id = 'xdebug-toggle';

        input.onclick = function () {
            if (this.checked) {
                self.enableXdebug();
            }
            else {
                self.disableXdebug();
            }
        };

        let label = document.createElement('label');
        label.for = 'xdebug-toggle';
        label.style = 'float: right; display: block; width: 100px; height: 100%; line-height: 37px;';
        label.classList = ['xdebug-toggle-container'];
        label.append(input);
        label.append('Xdebug');

    	let els = document.getElementsByClassName('urlbar');

    	let checkExist = setInterval(function() {
		   if (els.length) {
		      clearInterval(checkExist);
		      els[0].append(label);
		   }
		}, 100);

        return this;
    }

    enableXdebug() {
        let self = this;
        module.exports.requestHooks = [
            context => {
                context.request.setCookie('XDEBUG_SESSION', self.xdebugSessionKey);
            }
        ];
    }

    disableXdebug() {
        // set a dummy cookie to clear
        module.exports.requestHooks = [
            context => {
                context.request.setCookie('NO_XDEBUG_SESSION', 'none');
            }
        ];
    }

    destruct() {
        let els = document.getElementsByClassName('xdebug-toggle-container');

        if (els.length > 0) {
            for (let i = 0; i < els.length; i++) {
                els[i].remove();
            }
        }

        return this;
    }
}

let p = new InsomniaXdebugPlugin();
p.initPlugin();
